import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NbThemeModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ApiCoreService } from './shared/services/api-core.service';
import { CoreService } from './shared/services/core.service';
import { UtilitiesService } from './shared/services/utilities.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import { modul } from 'ngx-bootstrap';

@NgModule({
  id: 'app-module',
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NbThemeModule.forRoot({ name: 'custom_v2' }),
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [ApiCoreService, CoreService, UtilitiesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
