import { Component, OnInit } from '@angular/core';
import { TableProviderService } from '../../../shared/services/pharmacy/config/config.service';
import { ApiService } from './../../../shared/services/pharmacy/api.service';
import { NotificationsComponent } from './../../../shared/component/notifications/notifications.component';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent implements OnInit {
  private notify: NotificationsComponent;
  table = {
    data : [],
    settings : {},
    pageSize: 50,
    pageStep: 0
  };
  constructor(
    private tableService: TableProviderService,
    public apiService: ApiService,
  ) {

  }
  ngOnInit() {
    this.apiService.callAPI('get', 'pharmacy',
    ['warehouse', 'provider'],
    [('skip=' + this.table.pageStep), ('limit=' + this.table.pageSize)],
    {}).subscribe(
      res => {
        if (res.code === 200) {
          this.table.data = res.results;
        } else {
          this.notify.makeToast('error', res.status, res.message);
        }
      },
      err => {
        this.notify.makeToast('error', err.status, err.status);
      }
    );
  this.table.settings = this.tableService.set(this.table.data);
  }

}
