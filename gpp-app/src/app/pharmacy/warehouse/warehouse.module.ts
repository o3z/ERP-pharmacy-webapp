import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from './routing.module';
import { WarehouseComponent } from './warehouse.component';
import { DrugComponent } from './drug/drug.component';
import { MedicalComponent } from './medical/medical.component';
import { ProviderComponent } from './provider/provider.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import {
  TableDrugService,
  TableInvoiceInfoService,
  TableMedicalService,
  TableProviderService
} from './../../shared/services/pharmacy/config/config.service';
import { NbCardModule, NbActionsModule, NbLayoutModule } from '@nebular/theme';
import {
  ModalModule, TypeaheadModule, BsDatepickerModule, BsDropdownModule
} from 'ngx-bootstrap';
// import {
//   NgbDropdownModule, NgbPaginationModule, NgbModalModule, ModalDismissReasons,
//   NgbDatepickerModule
// } from '@ng-bootstrap/ng-bootstrap';
import { ImportComponent } from './modal-import/import.component';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { ComponentModule } from './../../shared/component/component.module';

@NgModule({
  id: 'pharmacy-warehouse-module',
  imports: [
    CommonModule,
    RoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbActionsModule,
    NbLayoutModule,
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    FileUploadModule,
    FormsModule,
    ComponentModule
  ],
  providers: [TableDrugService, TableInvoiceInfoService, TableMedicalService, TableProviderService],
  declarations: [WarehouseComponent, DrugComponent, MedicalComponent, ProviderComponent, ImportComponent ],
  bootstrap: []
})
export class WarehouseModule { }

