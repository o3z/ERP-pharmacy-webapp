import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DrugComponent } from './drug/drug.component';
import { MedicalComponent } from './medical/medical.component';
import { ProviderComponent } from './provider/provider.component';
import { WarehouseComponent } from './warehouse.component';

const routing: Routes = [
  {
    path: '', component: WarehouseComponent,
    children: [
      { path: '', redirectTo: 'drugs', pathMatch: 'full' },
      { path: 'drugs', component: DrugComponent },
      { path: 'medical-products', component: MedicalComponent },
      { path: 'providers', component: ProviderComponent },
    ]
  },
];

const Routing: ModuleWithProviders = RouterModule.forChild(routing);

@NgModule({
  id: 'pharmacy-warehouse-routing',
  imports: [
    CommonModule,
    Routing
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
  // providers: [AuthGuard]
})
export class RoutingModule { }
