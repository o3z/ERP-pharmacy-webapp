import { Component, OnInit } from '@angular/core';
import { TableDrugService } from '../../../shared/services/pharmacy/config/config.service';

@Component({
  selector: 'app-drug',
  templateUrl: './drug.component.html',
  styleUrls: ['./drug.component.css']
})
export class DrugComponent implements OnInit {
  table = {
    data : [],
    settings : {}
  };
  constructor(
    private tableService: TableDrugService,
  ) {
    this.table.settings = this.tableService.set(this.table.data);
  }

  ngOnInit() {
  }

}
