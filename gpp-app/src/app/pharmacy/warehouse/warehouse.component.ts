import { Component, OnInit, ViewChild } from '@angular/core';
import { ImportComponent } from './modal-import/import.component';
import { CommunicationService } from '../../shared/services/pharmacy/communication/communication.service';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.css'],
  providers: [ ],
})
export class WarehouseComponent implements OnInit {
  @ViewChild(ImportComponent) importInvoice: ImportComponent;

  routeDefault = '/pharmacy/warehouse/';
  menu = [
    {
      title: 'Thuốc',
      link: this.routeDefault + 'drugs',
      // icon: 'fas fa-pills',
    },
    {
      title: 'Sản phẩm y tế',
      link: this.routeDefault + 'medical-products',
      // icon: 'fab fa-product-hunt',
    },
    {
      title: 'Nhà cung cấp',
      link: this.routeDefault + 'providers',
      // icon: 'fas fa-truck',
    },
  ];
  sortOrders: string[] = ['Year', 'Title', 'Author'];
  selectedSortOrder: String = 'Sort by...';

  ChangeSortOrder(newSortOrder: string) {
    this.selectedSortOrder = newSortOrder;
  }
  constructor(
    private communication: CommunicationService,
  ) {
    this.communication.changeStateMenuTop(this.menu);
   }

  ngOnInit() {

  }
  openLg() {
    this.importInvoice.openModal();
  }
}
