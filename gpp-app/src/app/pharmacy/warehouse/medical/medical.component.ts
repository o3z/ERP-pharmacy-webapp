import { Component, OnInit, ViewChild } from '@angular/core';
import { TableMedicalService } from '../../../shared/services/pharmacy/config/config.service';
import { ApiService } from './../../../shared/services/pharmacy/api.service';
import { NotificationsComponent } from './../../../shared/component/notifications/notifications.component';

@Component({
  selector: 'app-medical',
  templateUrl: './medical.component.html',
  styleUrls: ['./medical.component.css']
})
export class MedicalComponent implements OnInit {
  @ViewChild(NotificationsComponent)
  private notify: NotificationsComponent;

  table = {
    data : [],
    settings : {},
    pageSize: 50,
    pageStep: 0
  };
  constructor(
    private tableService: TableMedicalService,
    public apiService: ApiService,
  ) {

  }

  ngOnInit() {
    this.apiService.callAPI('get', 'pharmacy',
    ['warehouse', 'Merchandise'],
    [('skip=' + this.table.pageStep), ('limit=' + this.table.pageSize)],
    {}).subscribe(
      res => {
        if (res.code === 200) {
          this.table.data = res.results;
        } else {
          this.notify.makeToast('error', res.status, res.message);
        }
      },
      err => {
        this.notify.makeToast('error', err.status, err.status);
      }
    );
  this.table.settings = this.tableService.set(this.table.data);
  }
}
