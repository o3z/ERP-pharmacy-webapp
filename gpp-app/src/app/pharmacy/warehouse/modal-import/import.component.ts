import { Component, OnInit, ViewChild, TemplateRef, OnChanges } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ErrorCodeService, TableInvoiceInfoService } from '../../../shared/services/pharmacy/config/config.service';
import * as Moment from 'moment';
import { ApiService } from './../../../shared/services/pharmacy/api.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css'],
  providers: [ErrorCodeService],
})

export class ImportComponent implements OnInit {
  moment = Moment;
  modalRef: BsModalRef;
  @ViewChild('template') private template: TemplateRef<any>;
  private subs = [];
  invoiceShort = false;
  invoiceFull = true;
  invoice = {
    code: '',
    time: new Date(),
    type: '',
    company: '',
    img: '',
    amount: 0
  };
  merchandise = {
    name: '',
    materialId: '',
    price: {
      in: 0,
      out: 0
    },
    priceOut: 0,
    lot: '',
    exp: new Date(),
    mfg: new Date(),
    quantity: 0,
    stock: 0,
    vat: 10,
    min: 0,
    total: 0
  };
  clickedItem: any;
  companys: Observable<[any]>;
  materials = [];
  merchandises = [];
  // const URL = '/api/';
  URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
  public hasBaseDropZoneOver = false;
  public uploader: FileUploader = new FileUploader({ url: this.URL });
  table = {
    data: new LocalDataSource,
    settings: {}
  };
  constructor(
    private modalService: BsModalService,
    public errorService: ErrorCodeService,
    private tableService: TableInvoiceInfoService,
    private api: ApiService
  ) {
    this.table.settings = this.tableService.get();
  }

  ngOnInit() {
    this.searchMaterial();
  }

  updateTable() {
    const sub = {...this.merchandise};
    sub.priceOut = sub.price.out;
    this.merchandises.push(sub);
    this.table.data.load(this.merchandises);
    console.log(this.merchandises);
  }
  openModal() {
    this.modalRef = this.modalService.show(this.template, { class: 'modal-xl' });
  }
  public fileOverBase(e: any): void {
    if (e) {
      this.hasBaseDropZoneOver = true;
    } else {
      this.hasBaseDropZoneOver = false;
    }
  }
  public showInvoiceShort() {
    // this.invoiceFull = false;
    // this.invoiceShort = true;
  }
  public showInvoiceFull() {
    // this.invoiceShort = false;
    // this.invoiceFull = true;
  }

  private searchMaterial() {
    // , 'filter=name:'+key
    this.api.callAPI('get', 'pharmacy', ['material'], ['fields=name company'], {}).subscribe(
      res => {
        if (res.code === 200) {
          this.materials = res.results;
          console.log(this.materials);
        } else {
        }
      },
      err => {
      }
    );
  }

}
