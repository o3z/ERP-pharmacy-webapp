import { Component, OnInit, ViewChild } from '@angular/core';
import { NbMenuService } from '@nebular/theme';
import { CommunicationService } from '../shared/services/pharmacy/communication/communication.service';

@Component({
  selector: 'app-pharmacy',
  templateUrl: './pharmacy.component.html',
  styleUrls: ['./pharmacy.component.css'],
  providers: [
    NbMenuService,
  ]

})
export class PharmacyComponent implements OnInit {
  menuItems = [
    {
      title: 'Dashboard',
      link: '/pharmacy/dashboard',
      // icon: 'fas fa-chart-line',
    },
    {
      title: 'Bán hàng',
      link: '/pharmacy/sale',
      // icon: 'fas fa-warehouse',
    },
    {
      title: 'Kho',
      link: '/pharmacy/warehouse',
      // icon: 'fas fa-warehouse',
    },
    {
      title: 'Cài đặt',
      link: '/pharmacy/account',
      // icon: 'fas fa-cog',
    },
  ];
  menuTop = [];
  constructor(
    private communication: CommunicationService
  ) {
    this.communication.menuTop.subscribe(res => {this.menuTop = res; });
  }

  ngOnInit() {

  }
  public reloadMenuTop() {

  }

}
