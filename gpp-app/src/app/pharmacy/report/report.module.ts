import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  id: 'pharmacy-report-module',
  imports: [
    CommonModule
  ],
  declarations: []
})
export class ReportModule { }
