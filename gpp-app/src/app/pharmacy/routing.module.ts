import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { PharmacyComponent } from './pharmacy.component';
import { AuthGuard } from './guard/auth.guard';

const routing: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: 'app/pharmacy/login/login.module#LoginModule' },
  { path: 'register', loadChildren: 'app/pharmacy/register/register.module#RegisterModule' },
  {
    path: '',
    component: PharmacyComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: 'app/pharmacy/account/account.module#AccountModule' },
      { path: 'sale', loadChildren: 'app/pharmacy/sale/sale.module#SaleModule' },
      { path: 'warehouse', loadChildren: 'app/pharmacy/warehouse/warehouse.module#WarehouseModule' },
      { path: 'report', loadChildren: 'app/pharmacy/warehouse/warehouse.module#WarehouseModule' },
      { path: 'staff', loadChildren: 'app/pharmacy/warehouse/warehouse.module#WarehouseModule' },
      { path: 'spharmacy', loadChildren: 'app/pharmacy/warehouse/warehouse.module#WarehouseModule' },
      { path: 'support', loadChildren: 'app/pharmacy/warehouse/warehouse.module#WarehouseModule' },
      { path: 'warehouse', loadChildren: 'app/pharmacy/warehouse/warehouse.module#WarehouseModule' },
      { path: 'account', loadChildren: 'app/pharmacy/account/account.module#AccountModule' },
    ],
    canActivateChild : [AuthGuard]
  },
];

const Routing: ModuleWithProviders = RouterModule.forChild(routing);

@NgModule({
  id: 'pharmacy-routing',
  imports: [
    CommonModule,
    Routing
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
  providers: [AuthGuard]
})
export class RoutingModule { }
