import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  id: 'pharmacy-support-module',
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SupportModule { }
