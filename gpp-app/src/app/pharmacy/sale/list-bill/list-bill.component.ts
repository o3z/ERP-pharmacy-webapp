import { Component, OnInit } from '@angular/core';
import { SaleService } from '../../../shared/services/pharmacy/communication/sale.service';

@Component({
  selector: 'app-list-bill',
  templateUrl: './list-bill.component.html',
  styleUrls: ['./list-bill.component.css']
})
export class ListBillComponent implements OnInit {
  public invoices = [{
    patient: {
      name: 'nguyễn văn a',
      gender: 'M',
      phoneNumber: '841648375392',
    },
    code: '123456',
    time: '10:10 10/02/2018',
    totalAmount: 100000,
    staff: {
      name: 'không biết'
    },
    creater: '1d56a4das489d554a6',
  },
  {
    patient: {
      name: 'nguyễn văn b',
      gender: 'M',
      phoneNumber: '841648375392',
    },
    code: '123456',
    time: '10:10 10/02/2018',
    totalAmount: 100000,
    staff: {
      name: 'không biết'
    },
    creater: '1d56a4das489d554a6',
  },
  {
    patient: {
      name: 'nguyễn văn c',
      gender: 'M',
      phoneNumber: '841648375392',
    },
    code: '123456',
    time: '10:10 10/02/2018',
    totalAmount: 100000,
    staff: {
      name: 'không biết'
    },
    creater: '1d56a4das489d554a6',
  },
  {
    patient: {
      name: 'nguyễn văn d',
      gender: 'M',
      phoneNumber: '841648375392',
    },
    code: '123456',
    time: '10:10 10/02/2018',
    totalAmount: 100000,
    staff: {
      name: 'không biết'
    },
    creater: '1d56a4das489d554a6',
  },
  {
    patient: {
      name: 'nguyễn văn e',
      gender: 'M',
      phoneNumber: '841648375392',
    },
    code: '123456',
    time: '10:10 10/02/2018',
    totalAmount: 100000,
    staff: {
      name: 'không biết'
    },
    creater: '1d56a4das489d554a6',
  },
  {
    patient: {
      name: 'nguyễn văn a',
      gender: 'M',
      phoneNumber: '841648375392',
    },
    code: '123456',
    time: '10:10 10/02/2018',
    totalAmount: 100000,
    staff: {
      name: 'không biết'
    },
    creater: '1d56a4das489d554a6',
  }];

  constructor(
    private communication: SaleService,
  ) {

  }

  ngOnInit() {
  }

  public updateList() {
// TODO: CALLAPI update invoices
  }
  public activeBill(item) {
    this.communication.chooseInvoice(item);
  }
}
