import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbActionsModule, NbLayoutModule, NbTabsetModule } from '@nebular/theme';
import {
  TypeaheadModule,
  ButtonsModule,
} from 'ngx-bootstrap';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SaleRoutingModule } from './sale-routing.module';
import { SaleComponent } from './sale.component';
import { SearchItemComponent } from './search-item/search-item.component';
import { BillComponent } from './bill/bill.component';
import { ListBillComponent } from './list-bill/list-bill.component';
import { FormsModule } from '@angular/forms';
import { SaleService } from '../../shared/services/pharmacy/communication/sale.service';

@NgModule({
  imports: [
    CommonModule,
    SaleRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbActionsModule,
    NbLayoutModule,
    TypeaheadModule.forRoot(),
    ButtonsModule.forRoot(),
    FormsModule,
    NbTabsetModule
  ],
  declarations: [SaleComponent, SearchItemComponent, BillComponent, ListBillComponent],
  providers: [SaleService]
})
export class SaleModule { }
