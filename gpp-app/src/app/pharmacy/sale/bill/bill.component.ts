import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { TableSaleItemService } from '../../../shared/services/pharmacy/config/table-sale-item.service';
import { SaleService } from '../../../shared/services/pharmacy/communication/sale.service';
import { ListBillComponent } from '../list-bill/list-bill.component';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css'],
  providers: [TableSaleItemService]
})
export class BillComponent implements OnInit, OnDestroy {
  @ViewChild(ListBillComponent) private listBill: ListBillComponent;
  private invoice = {
    patient: {
      name: 'nguyễn văn a',
      gender: 'M',
      phoneNumber: '841648375392',
    },
    code: '123456',
    time: '10:10 10/02/2018',
    totalAmount: 100000,
    staff: {
      name: 'không biết'
    },
    creater: '1d56a4das489d554a6',
    details: []
  };
  private subs = [];

  public table = {
    data: new LocalDataSource,
    settings: {}
  };

  constructor(
    private tableService: TableSaleItemService,
    private communication: SaleService
  ) {
    this.table.settings = this.tableService.get();
    this.subs.push(this.communication.invoice.subscribe((invoice) => {
      this.invoice = invoice;
      console.log(this.invoice);
    }));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      if (sub) { sub.unsubscribe(); }
    });
  }
  public updateDetail(item) {
    this.invoice.details.push(item);
    this.table.data.prepend(item);
    this.table.data.refresh();
    console.log(this.invoice.details);
  }
  public removeDetail(item) {
    this.invoice.details.splice(this.invoice.details.indexOf(item), 1);
    this.table.data.remove(item);
    this.table.data.refresh();
  }
  public updateInvoice() {
    // TODO: callAPI update invoice
  }
  public loadInvoice(invoice) {
    this.table.data.getAll().then((value) => {
      this.invoice.details = value;
      this.table.data.empty();
      this.invoice = invoice;
    }).catch((err) => {
      console.log(err);
    });
  }
  public completeInvoice() {
  // TODO: call API success
    this.listBill.updateList();
  }
  public saveInvoice() {
    // TODO: snackbar save success
  }
}
