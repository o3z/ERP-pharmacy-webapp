import { Component, OnInit, ViewChild } from '@angular/core';
import 'rxjs/add/observable/of';
import { SaleService } from '../../../shared/services/pharmacy/communication/sale.service';
import { BillComponent } from '../bill/bill.component';

@Component({
  selector: 'app-search-item',
  templateUrl: './search-item.component.html',
  styleUrls: ['./search-item.component.css']
})
export class SearchItemComponent implements OnInit {
  @ViewChild(BillComponent) private bill: BillComponent;
  public categories = [
    {
      name: 'tim mạch'
    },
    {
      name: 'dị ứng'
    },
    {
      name: 'thực phẩm chức năng'
    },
    {
      name: 'cảm sốt ho'
    },
    {
      name: 'cho bé'
    }
  ];
  public items = [
    {
      search: 'abc xyz',
      code: '0000',
      lot: 'abcxyz',
      exp: '1/1/2020',
      mfg: '1/1/2000',
      stock: '15',
      min: '5',
      img: ''
    },
    {
      search: '123 abc xyz',
      code: '0001',
      lot: 'abcxyz',
      exp: '1/1/2020',
      mfg: '1/1/2000',
      stock: '15',
      min: '5',
      img: ''
    },
    {
      search: '123 abc xyz dash djas dj ha hdksha',
      code: '0001',
      lot: 'abcxyz',
      exp: '1/1/2020',
      mfg: '1/1/2000',
      stock: '15',
      min: '5',
      img: ''
    },
    {
      search: '123 abc xyz',
      code: '0001',
      lot: 'abcxyz',
      exp: '1/1/2020',
      mfg: '1/1/2000',
      stock: '15',
      min: '5',
      img: ''
    },
    {
      search: '123 abc xyz',
      code: '0001',
      lot: 'abcxyz',
      exp: '1/1/2020',
      mfg: '1/1/2000',
      stock: '15',
      min: '5',
      img: ''
    },
    {
      search: '123 abc xyz',
      code: '0001',
      lot: 'abcxyz',
      exp: '1/1/2020',
      mfg: '1/1/2000',
      stock: '15',
      min: '5',
      img: ''
    },
    {
      search: '123 abc xyz',
      code: '0001',
      lot: 'abcxyz',
      exp: '1/1/2020',
      mfg: '1/1/2000',
      stock: '15',
      min: '5',
      img: ''
    },
    {
      search: '123 abc xyz',
      code: '0001',
      lot: 'abcxyz',
      exp: '1/1/2020',
      mfg: '1/1/2000',
      stock: '15',
      min: '5',
      img: ''
    }
  ];
  categoryChoosed = {};
  itemChoosed = {};
  constructor(
    public communication: SaleService
  ) {

   }

  ngOnInit() {
  }
  public addDetail(item) {
    this.bill.updateDetail(item);
    console.log(item);
  }
  public removeDetail(item) {
    this.bill.removeDetail(item);
  }

}
