import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from './../../shared/services/pharmacy/api.service';
import { NotificationsComponent } from './../../shared/component/notifications/notifications.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorCodeService } from './../../shared/services/pharmacy/config/config.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild(NotificationsComponent)
  private notify: NotificationsComponent;
  info = {
    username: '',
    password: ''
  };
  checkSave = false;
  checkLoading = false;
  verify = '';

  constructor(
    public apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    public errorService: ErrorCodeService,
  ) {
    this.route.queryParams.subscribe(res => { this.verify = res.verify; });
  }

  ngOnInit() {
  }

  /**
 * TEST: verify OTP
 */
  public confirmVerify(data: any) {
    this.errorService.active();
    const query = this.verify ? ('otp=' + this.verify) : '';
    this.checkLoading = !this.checkLoading;

    this.checkLoading = true;
    if (!this.errorService.get(['username', 'password'])) {
      this.apiService.callAPI('post', 'pharmacy/register/step3', [], [query], data).subscribe(
        res => {
          this.apiService.setHeaders('token', res.results);
          if (res.code === 200) {
            this.notify.makeToast('success', res.status, res.message);
            localStorage.setItem('currentUser', res.results.user);
          } else {
            this.checkLoading = false;
            this.notify.makeToast('error', res.status, res.message);
          }
        },
        (err) => {
          this.notify.makeToast('error', err.status, err.status);
          this.checkLoading = false;
        }
      );
    } else {
      this.checkLoading = false;
    }

  }
  /**
   * TEST: LOGIN
   * @param data;
   */
  public login(data: any) {
    this.errorService.active();
    this.checkLoading = !this.checkLoading;
    const user = {
      username: data.username,
      password: data.password,
    };

    this.checkLoading = true;
    if (!this.errorService.get(['username', 'password'])) {
      this.apiService.callAPI('post', 'login', [], [], user).subscribe(
        res => {
          this.apiService.setHeaders('token', res.results.token);
          if (res.code === 200) {
            localStorage.setItem('currentUser', res.results.user);
            this.notify.makeToast('success', res.status, res.message);
            this.goto('pharmacy', {});
          } else {
            this.checkLoading = false;
            this.notify.makeToast('error', res.status, res.message);
          }
        },
        (err) => {
          this.notify.makeToast('error', err.status, err.status);
          this.checkLoading = false;
        }
      );
    } else {
      this.checkLoading = false;
    }
  }
  /**
   * Hàm direct to route cho template
   * @param path direct to route
   * @param data for direct that
   */
  goto(path, data) {
    this.router.navigate([path, data]);
  }
}
