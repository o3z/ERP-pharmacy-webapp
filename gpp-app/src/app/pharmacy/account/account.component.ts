import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../../shared/services/pharmacy/communication/communication.service';
import { ApiService } from '../../shared/services/pharmacy/api.service';
// import { NotificationsComponent } from './../../shared/component/notifications/notifications.component';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  // @ViewChild(NotificationsComponent)
  routeDefault = '/pharmacy/account/';
  menu = [
    {
      title: 'Thông tin cá nhân',
      link: this.routeDefault,
      // icon: 'fas fa-info',
    },
    {
      title: 'Thẻ ngân hàng',
      link: this.routeDefault + 'credit',
      // icon: 'far fa-money-bill-alt',
    },
    {
      title: 'Mật khẩu',
      link: this.routeDefault + 'password',
      // icon: 'fas fa-unlock-alt',
    }
  ];
  id: any;
  public staff: {
    userInfo: {
      _id: '',
      name: '',
      gender: 'U',
      birthday: '',
      birthdayDefault: '',
      phone: {
        number: ''
      },
      email: ''
    }
  };
  constructor(
    private communication: CommunicationService,
    private apiService: ApiService
  ) {
    this.communication.changeStateMenuTop(this.menu);
    // this.id = localStorage.getItem('currentUser');
  }

  ngOnInit() {
    // this.loadPage(this.id);

  }
  loadPage(id) {
    this.apiService.getStaff(id).subscribe(
      res => {
        if (res.code === 200) {
          this.staff = res.results;
        }
      });

  }


}
