import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './account.component';
import { PasswordComponent } from './password/password.component';
import { InfoComponent } from './info/info.component';

const routing: Routes = [
  { path: '', component: AccountComponent,
  children: [
    { path: '', redirectTo: 'account', pathMatch: 'full' },
    { path: 'info', component: InfoComponent },
    { path: 'account', component: AccountComponent },
    { path: 'password', component: PasswordComponent },
  ]
}];

const Routing: ModuleWithProviders = RouterModule.forChild(routing);

@NgModule({
  imports: [
    CommonModule,
    Routing
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingAccountModule { }
