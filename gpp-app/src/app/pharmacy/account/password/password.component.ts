import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../shared/services/pharmacy/api.service';
import { NotificationsComponent } from './../../../shared/component/notifications/notifications.component';
declare var $: any;

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {



     staff = {
    userInfo: {
      _id: '',
      password: '',
      name: '',
    },
    staffInfo: {}
  }
  id: string;
  notify: NotificationsComponent;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }
  ngOnInit() {
    this.id = localStorage.getItem('currentUser');
    this.loadPage(this.id);
  }
  loadPage(id) {
    this.apiService.getStaff(id).subscribe(
      res => {
        if (res.code == 200) {
          this.staff = res.results;
          console.log(this.staff)
        }
      })

  }
  updateStaff() {
    this.apiService.updateStaff(this.staff.userInfo._id, this.staff).subscribe(
      res => {
        if (res.code == 200) {
          this.notify.makeToast('success', res.status, res.message);
        } else {
          this.notify.makeToast('error', res.status, res.message);
        }
      })
  }
}
