import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../shared/services/pharmacy/api.service';
import { NotificationsComponent } from './../../../shared/component/notifications/notifications.component';
declare var $: any;

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
  providers: [ApiService]
})
export class InfoComponent {
  @ViewChild(NotificationsComponent) private notify: NotificationsComponent;
  @Input() public staff = {
    userInfo: {
      name: "",
      _id: "",
      gender: "",
      birthday: "",
      phone:{
        number: "",
        code: ""
      },
      email: ""
    }
  };

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }
  updateStaff() {
    this.apiService.updateStaff(this.staff.userInfo._id, this.staff).subscribe(
      res => {
        if (res.code === 200) {
          this.notify.makeToast('success', res.status, res.results);
        } else {
          this.notify.makeToast('error', res.status, res.results);
        }
      }
    );

  }
}
