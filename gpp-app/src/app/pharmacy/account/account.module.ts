import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingAccountModule } from './routing.module';
import { PasswordComponent } from './password/password.component';
import { InfoComponent } from './info/info.component';
import { FormsModule } from '@angular/forms';
import { AccountComponent } from './account.component';
import {  BsDatepickerModule} from 'ngx-bootstrap';

@NgModule({
  id: 'pharmacy-account-module',
  imports: [
    CommonModule, RoutingAccountModule,
    FormsModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [AccountComponent, PasswordComponent, InfoComponent]
})
export class AccountModule { }
