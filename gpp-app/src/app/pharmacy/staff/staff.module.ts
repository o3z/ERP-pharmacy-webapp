import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  id: 'pharmacy-staff-module',
  imports: [
    CommonModule
  ],
  declarations: []
})
export class StaffModule { }
