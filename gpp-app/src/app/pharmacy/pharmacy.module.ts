import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from './routing.module';
import { PharmacyComponent } from './pharmacy.component';
import { NbLayoutModule, NbSidebarModule, NbMenuModule, NbSidebarService, NbCardModule, NbActionsModule} from '@nebular/theme';
import { ApiService } from './../shared/services/pharmacy/api.service';
import { ComponentModule } from './../shared/component/component.module';
import { CommunicationService } from '../shared/services/pharmacy/communication/communication.service';

@NgModule({
  id: 'pharmacy-module',
  imports: [
    CommonModule,
    RoutingModule,
    NbSidebarModule.forRoot(),
    NbLayoutModule,
    NbMenuModule.forRoot(),
    NbCardModule,
    NbActionsModule,
    ComponentModule,
    // Ng2SmartTableModule,
  ],
  declarations: [PharmacyComponent],
  providers: [ApiService, CommunicationService, NbSidebarService]
})
export class PharmacyModule { }
