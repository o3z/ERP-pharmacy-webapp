import { Component, OnInit , OnDestroy} from '@angular/core';
import { ApiService } from './../../../shared/services/pharmacy/api.service';
import { CommunicationService } from '../../../shared/services/pharmacy/communication/communication.service';
import { ErrorCodeService } from '../../../shared/services/pharmacy/config/config.service';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.css'],
  // providers: [ApiService]
})
export class Step1Component implements OnInit, OnDestroy {
  whatStep = 'step1';
  subs = [];
  user = {
    name: '',
    gender: 'U',
    birthday: new Date(),
    phone: {
      code: '+84',
      number: '',
      codeCountry: 'vi-VN'
    },
    email: '',
    username: '',
    password: '',
    checkTerm: false,
    repassword : '',
  };
  constructor(
    private communication: CommunicationService,
    public errorService: ErrorCodeService,
  ) {
    this.communication.changeUser(this.user);
    this.communication.registerChangeStep(this.whatStep);
  }

  ngOnInit() {
  }
  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
