import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../../shared/services/pharmacy/api.service';
import { CommunicationService } from '../../../shared/services/pharmacy/communication/communication.service';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.css']
})
export class Step3Component implements OnInit {
  whatStep = 'step3';
  countdown = 0;
  constructor(
    private apiService: ApiService,
    private communication: CommunicationService,
  ) {
    this.communication.registerChangeStep(this.whatStep);
    this.countdowner(10000, 1000, function() {
      apiService.goto('login', {});
    });
  }
  ngOnInit() {
  }
  countdowner(time, delay, cb) {
    const self = this;
    self.countdown = time / delay;
    const interval = window.setInterval(function() {
      time -= delay;
      self.countdown = time / delay;
      if (time < 1) {
        clearInterval(interval);
        if (cb) { cb(); }
       }
    }, delay);
  }
}
