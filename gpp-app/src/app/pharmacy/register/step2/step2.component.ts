import { Component, OnInit , ViewChild} from '@angular/core';
import { CommunicationService } from '../../../shared/services/pharmacy/communication/communication.service';
import { ErrorCodeService } from '../../../shared/services/pharmacy/config/config.service';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.css']
})
export class Step2Component implements OnInit {
  whatStep = 'step2';
  drugStore = {
    name: ''
  };
  constructor(
    private communication: CommunicationService,
    public errorService: ErrorCodeService,
  ) {
    this.communication.registerChangeDrugStore(this.drugStore);
    this.communication.registerChangeStep(this.whatStep);
  }

  ngOnInit() {
  }

}
