import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ApiService } from '../../shared/services/pharmacy/api.service';
import { CommunicationService } from '../../shared/services/pharmacy/communication/communication.service';
import { NotificationsComponent } from './../../shared/component/notifications/notifications.component';
import { assign } from 'lodash';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorCodeService } from '../../shared/services/pharmacy/config/config.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  @ViewChild(NotificationsComponent) private notify: NotificationsComponent;

  whatStep = 'step1';
  subs = [];
  drugStore = {};
  stateBtn = {
    step2 : {
      isLoading : false,
    }
  };
  user = {
    name: '',
    gender: 'U',
    birthday: new Date(),
    phone: {
      code: '',
      number: ''
    },
    email: '',
    username: '',
    password: '',
    checkTerm: false,
    repassword : '',
  };
  constructor(
    private apiService: ApiService,
    private communication: CommunicationService,
    private route: ActivatedRoute,
    private router: Router,
    public errorService: ErrorCodeService,
  ) {
    this.subs.push(this.communication.user.subscribe(user => {
      this.user = user;
    }));
    this.subs.push(this.communication.whatStep.subscribe(step => {
      this.whatStep = step;
    }));
    this.subs.push(this.communication.drugStore.subscribe(drugStore => {
      this.drugStore = drugStore;
    }));
  }

  ngOnInit() {
  }
  ngOnDestroy() {
    this.subs.forEach(sub => {
      if (sub) { sub.unsubscribe(); }
    });
  }
  step1() {
    this.errorService.active();
    const self = this;
    // console.log(this.errorService.getAll());
    if (!this.errorService.getAll()) {
      this.apiService.registerStep1(this.user).subscribe(
        res => {
          if (res.results.step) {
            this.goto('pharmacy/register/step2', {});
          }
        },
        err => {
          this.notify.makeToast('error', err.status, err.message);
        }
      );
    }
  }
  step2() {
    this.errorService.active();
    if (!this.errorService.getAll()) {
      const data = assign(this.user, { drugStore: this.drugStore });
      this.apiService.registerStep2(data).subscribe(
        res => {
          this.goto('pharmacy/register/step3', {});
          this.stateBtn.step2.isLoading = true;
        },
        err => {
          this.stateBtn.step2.isLoading = false;
          this.notify.makeToast('error', err.status, err.message);
        }
      );
    }
  }
  goto(path, data) {
    this.router.navigate([path, data]);
  }
}
