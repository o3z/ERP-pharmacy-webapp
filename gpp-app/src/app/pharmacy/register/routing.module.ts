import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';

const routing: Routes = [
    { path: '', component: RegisterComponent,
    children: [
      { path: '', redirectTo: 'step1', pathMatch: 'full'},
      { path: 'step1', component: Step1Component},
      { path: 'step2', component: Step2Component},
      { path: 'step3', component: Step3Component},
      // { path: 'step3', loadChildren: './../login/login.module#LoginModule'},
    ],
  }
];

const Routing: ModuleWithProviders = RouterModule.forChild(routing);

@NgModule({
  id: 'pharmacy-register-routing',
  imports: [
    CommonModule,
    Routing
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
