import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NbLayoutModule, NbCardModule, NbCheckboxModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { ComponentModule } from './../../shared/component/component.module';
import { RoutingModule } from './/routing.module';
import { RegisterComponent } from './register.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { ApiService } from '../../shared/services/pharmacy/api.service';
// import { LoginModule } from './../login/login.module';
import { ErrorCodeService } from '../../shared/services/pharmacy/config/config.service';

@NgModule({
  id: 'pharmacy-register-module',
  imports: [
    CommonModule,
    RoutingModule,
    NbLayoutModule,
    NbCardModule,
    NbCheckboxModule,
    FormsModule,
    ComponentModule,
    // LoginModule
  ],
  declarations: [RegisterComponent, Step1Component, Step2Component, Step3Component],
  bootstrap: [RegisterComponent],
  providers: [ApiService, ErrorCodeService]
})
export class RegisterModule { }
