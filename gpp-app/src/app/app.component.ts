import { Component } from '@angular/core';
import { routerTransition} from './shared/libs/routerTransition';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [ routerTransition ],
})
export class AppComponent  {
  title = 'Nhà thuốc GPP';
  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
}
