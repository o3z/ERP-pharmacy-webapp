import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routing: Routes = [
  {path: '', redirectTo: 'pharmacy', pathMatch: 'full' },
  {path: 'pharmacy', loadChildren: 'app/pharmacy/pharmacy.module#PharmacyModule'},
  {path: 'customer', loadChildren: 'app/customer/customer.module#CustomerModule'},
];

const Routing: ModuleWithProviders = RouterModule.forRoot(routing);

@NgModule({
  id: 'app-routing',
  imports: [
    CommonModule,
    Routing
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
