import { Component, OnInit, ViewChild} from '@angular/core';
import { ApiCustomerService } from '../../shared/services/api-customer.service';
import { NotificationsComponent } from './../../shared/component/notifications/notifications.component';
import { ActivatedRoute, Router } from '@angular/router';
import { pad } from 'lodash';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [ApiCustomerService]
})
export class RegisterComponent implements OnInit {
  @ViewChild(NotificationsComponent)
  private notify: NotificationsComponent;
  isLoading = false;
  checkTerm = false;
  isSuccess = false;
  countdown = 0;
  info = {
    name: '',
    gender: 'U',
    birthday: new Date(),
    phone: {
      code: '',
      number: ''
    },
    email: '',
    username: '',
    password: '',
    checkTerm: false,
    repassword : ''
  };
  constructor(
    public apiCustomerService: ApiCustomerService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
  }

  register() {
    const self = this;
    this.info.checkTerm = this.checkTerm;
    this.isLoading = true;
    this.apiCustomerService.register(this.info).subscribe(
      res => {
        this.notify.makeToast('success', res.status, res.message);
        this.isSuccess = true;
        this.countdowner(5000, 1000, function() {
          self.goto('customer/login', {});
        });
      },
      err => {
        this.isLoading = false;
        this.notify.makeToast('error', err.status, err.message);
      }
    );
  }
  goto(path, data) {
    this.router.navigate([path, data]);
  }
  countdowner(time, delay, cb) {
    const self = this;
    self.countdown = time / delay;
    const interval = window.setInterval(function() {
      time -= delay;
      self.countdown = time / delay;
      if (time < 1) {
        clearInterval(interval);
        if (cb) { cb(); }
       }
    }, delay);
  }
}
