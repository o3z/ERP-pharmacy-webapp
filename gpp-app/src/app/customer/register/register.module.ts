import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NbLayoutModule, NbCardModule, NbCheckboxModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { ComponentModule } from './../../shared/component/component.module';
import { RegisterComponent } from './register.component';

const routing: Routes = [
  { path: '', component: RegisterComponent }
];
const Routing: ModuleWithProviders = RouterModule.forChild(routing);

@NgModule({
  id: 'customer-register-module',
  imports: [
    CommonModule,
    Routing,
    NbLayoutModule,
    NbCardModule,
    NbCheckboxModule,
    FormsModule,
    ComponentModule
  ],
  declarations: [RegisterComponent]
})
export class RegisterModule { }
