import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NbThemeService } from '@nebular/theme';

import { CustomerComponent } from './customer.component';

const routing: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren:  'app/customer/login/login.module#LoginModule' },
  { path: 'register', loadChildren:  'app/customer/register/register.module#RegisterModule' },
  { path: 'dashboard', component: CustomerComponent },
];
const Routing: ModuleWithProviders = RouterModule.forChild(routing);

@NgModule({
  id: 'customer-routing',
  imports: [
    CommonModule,
    Routing
    ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule {
  // constructor(private themeService: NbThemeService) {
  //   this.themeService.changeTheme('customer');
  // }
}
