import { Component, OnInit } from '@angular/core';
import {NbMenuService, NbMenuItem } from '@nebular/theme';
// import { n } from '@nebular/theme/components/menu/menu.component';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
  providers: [
    NbMenuService
  ]

})
export class CustomerComponent implements OnInit {

  menuItems = [
    {
      title: 'Menu Items',
      group: true,
      icon: 'nb-keypad',
    },
    {
      title: 'Menu #1',
      link: '/menu/1',
      icon: 'nb-keypad',
      queryParams: { param: 1 },
      fragment: '#fragment',
    },
    {
      title: 'Menu #2',
      link: '/menu/2',
      icon: 'nb-keypad',
    },
  ];
  constructor(
    private nbMenuService: NbMenuService
  ) {
  // this.nbMenuService.addItems(this.items,  '');
    
  }

  ngOnInit() {
    
  }

}
