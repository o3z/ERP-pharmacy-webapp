import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NbLayoutModule, NbCardModule, NbCheckboxModule } from '@nebular/theme';
import { ComponentModule } from './../../shared/component/component.module';
import { LoginComponent } from './login.component';
import { RoutingModule } from './/routing.module';

@NgModule({
  id: 'customer-login-module',
  imports: [
    CommonModule,
    RoutingModule,
    NbLayoutModule,
    NbCardModule,
    NbCheckboxModule,
    FormsModule,
    ComponentModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
