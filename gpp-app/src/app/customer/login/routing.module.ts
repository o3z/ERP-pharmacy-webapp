import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';

const routing: Routes = [
  { path: '', component:  LoginComponent },
];
const Routing: ModuleWithProviders = RouterModule.forChild(routing);

@NgModule({
  id: 'customer-login-routing',
  imports: [
    CommonModule,
    Routing
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
