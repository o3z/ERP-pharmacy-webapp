import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiCustomerService } from './../../shared/services/api-customer.service';
import { NotificationsComponent } from './../../shared/component/notifications/notifications.component';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [
    ApiCustomerService
  ]
})
export class LoginComponent implements OnInit {
  @ViewChild(NotificationsComponent)
  private notify: NotificationsComponent;
  info = {
    username: '',
    password: ''
  };
  checkSave = false;
  checkLoading = false;
  verify = '';
  constructor(
    public api: ApiCustomerService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.queryParams.subscribe(res => {this.verify = res.verify; });
  }

  ngOnInit() {
  }
  public confirmVerify(data: any) {
    /**
     * TEST: verify OTP
     */
    const query = this.verify ? ('otp=' + this.verify) : '';
    this.checkLoading = !this.checkLoading;
    const user = {
      username: data.username,
      password: data.password,
    };
        /**
     * TEST: LOGIN
     */
    this.api.callAPI('post', 'customer/verify', [], [query], user).subscribe(
      res => {
        this.api.setHeaders('token', res.results);
        this.checkLoading = !this.checkLoading;
        if (res.code === 200) {  
          this.notify.makeToast('success', res.status, res.message);
        } else {
          this.notify.makeToast('error', res.status, res.message);
        }
      },
      (err) => {
        this.notify.makeToast('error', err.status, err.status);
        this.checkLoading = !this.checkLoading;
      }
    );
  }
  public login(data: any) {
    this.checkLoading = !this.checkLoading;
    const user = {
      username: data.username,
      password: data.password,
    };
    /**
     * TEST: LOGIN
     */
    this.api.callAPI('post', 'login', [], [], user).subscribe(
      res => {
        this.api.setHeaders('token', res.results);
        this.checkLoading = !this.checkLoading;
        if (res.code === 200) {
          this.router.navigate(['/customer/dashboard']);
          this.notify.makeToast('success', res.status, res.message);
        } else {
          this.notify.makeToast('error', res.status, res.message);
        }
      },
      (err) => {
        this.notify.makeToast('error', err.status, err.status);
        this.checkLoading = !this.checkLoading;
      }
    );
  }
/**
 * Hàm direct to route cho template
 * @param path direct to route
 * @param data for direct that
 */
  goto(path, data) {
    this.router.navigate([path, data]);
  }
}
