import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NbLayoutModule, NbSidebarModule, NbMenuModule, NbSidebarService} from '@nebular/theme';
import { FormsModule } from '@angular/forms';

import { CustomerComponent } from './customer.component';
import { RoutingModule } from './/routing.module';

@NgModule({
  id: 'customer-module',
  imports: [
    CommonModule,
    RoutingModule,
    FormsModule,
    NbSidebarModule.forRoot(),
    NbLayoutModule,
    NbMenuModule.forRoot()
  ],
  declarations: [CustomerComponent],
  providers: [NbSidebarService ]
})
export class CustomerModule { }
