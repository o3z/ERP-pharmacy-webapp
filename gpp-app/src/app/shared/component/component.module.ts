import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToasterModule } from 'angular2-toaster';
import { NotificationsComponent} from './notifications/notifications.component';

@NgModule({
  id: 'shared-component',
  imports: [
    CommonModule,
    ToasterModule.forRoot()
  ],
  declarations: [NotificationsComponent],
  exports: [NotificationsComponent]
})
export class ComponentModule { }
