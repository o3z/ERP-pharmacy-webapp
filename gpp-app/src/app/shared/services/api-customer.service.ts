import { Injectable } from '@angular/core';
import { ApiCoreService } from './api-core.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class ApiCustomerService extends ApiCoreService {
  constructor(
    protected http: HttpClient,
  ) {
    super(http);
    const defaults = [];
    this.initHeaderDefault(defaults);
    this.apiHost = environment.apiEndpoint;
  }
  public setHeaders(key: string, value) {
    this.headers = this.headers.append(key, value);
  }
  register(user: any) {
    return this.callAPI('post', 'customer/register', [], [], user);
  }
}
