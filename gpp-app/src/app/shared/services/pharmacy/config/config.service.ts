// import { Injectable } from '@angular/core';
export { TableDrugService } from './table-drug.service';
export { TableMedicalService } from './table-medical.service';
export { TableProviderService } from './table-provider.service';
export { ErrorCodeService } from './error-code.service';
export { TableInvoiceInfoService } from './table-invoice-info.service';

