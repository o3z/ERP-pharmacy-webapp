import { Injectable, Input } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

@Injectable()
export class TableInvoiceInfoService {
  private data: LocalDataSource;
  private settings = {
    mode: 'inline',
    // hideHeader: true,
    hideSubHeader: true,
    noDataMessage: 'Chưa có dữ liệu',
    actions: {
      columnTitle: '',
      add: false,
      position: 'right',
    },
    edit: {
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="far fa-save"></i>',
      cancelButtonContent: '<i class="fas fa-ban"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="far fa-trash-alt"></i>',
    },
    pager: {
      display: true,
      perPage: 50,
    },
    attr: {
      class: 'table table-bordered'
    },
    sort: true,
    columns: {
      name: {
        title: 'Tên sản phẩm',
        type: 'html',
        filter: {
          type: 'html',
          config: {
            completer: {
              data: this.data,
              searchFields: 'name',
              titleField: 'name',
            },
          },
        }
      },
      lot: {
        title: 'Số lô',
        filter: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'type',
              titleField: 'type',
            },
          },
        },
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'type',
              titleField: 'type',
            },
          },
        },
      },
      exp: {
        title: 'Hạn sử dụng',
        filter: {
          type: 'text',
          config: {
            completer: {
              data: this.data,
              searchFields: 'lot',
              titleField: 'lot',
            },
          },
        },
      },
      priceOut: {
        title: 'Đơn giá',
        type: 'completer',
        renderComponent: '',

      },
      quantity: {
        title: 'Số lượng nhập',
        filter: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'exp',
              titleField: 'exp',
            },
          },
        },
        sortDirection: 'asc',
      },
      total: {
        title: 'Tổng',
        filter: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'exp',
              titleField: 'exp',
            },
          },
        },
        sortDirection: 'asc',
      },
    }
  };
  constructor() {}

  public get(): Object {
    return this.settings;
  }
}
