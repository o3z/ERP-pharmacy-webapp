/**
 * Service quản lý nội dung lỗi trên FE
 * cách dùng:
 * Mỗi mã lỗi tương ứng có một function để xác định lỗi
 * Mỗi function có 1 params cuối cùng là "tag" - dùng để đánh dấu trạng thái lỗi của input
 * Để lấy trạng thái lỗi của input từ service gọi đến function get(tag) kiểu dữ liệu trả về true/false
 * Có 2 function active và deactive dùng để bật/tắt services, mặc định sẽ là tắt
 */
import { Injectable } from '@angular/core';
import { isNil, lowerCase, isEqual } from 'lodash';
import * as moment from 'moment';
import * as validator from 'validator';
import { remove, slice } from 'lodash';

@Injectable()
export class ErrorCodeService {

  private keys = [];
  private helpText = {};
  private HELPTEXT = {
    100: 'Bắt buộc không để trống',
    101: 'Giới tính trong CMND',
    102: 'Ngày sinh phải là ngày ở quá khứ',
    103: 'Sai định dạng số điện thoại.',
    // 104: 'Số điện thoại đã đã được đăng ký',
    105: 'Sai định dạng email. VD: abc@xyz.com',
    // 106: 'Email đã được đăng ký',
    107: 'Sai định dạng. Tên tài khoản viết liền, không dấu, không chứa kí tự đặc biệt !@#$%^&*()...',
    // 108: 'Tài khoản đã được sử dụng, hãy thử tên tài khoản khác',
    109: 'Bắt buộc phải đồng ý điều điện và điều khoản sử dụng dịch vụ.',
    110: 'Mật khẩu nhập lại không trùng khớp với mật khẩu',
    111: 'Quý nhà thuốc chưa đồng ý "điều kiện và điều khoản sử dụng dịch vụ"',
  };

  constructor() { }

  private register(tag: string) {
    const index = this.keys.indexOf(tag);
    if (index < 0) {
      this.keys.push(tag);
    }
  }
  private unregister(tag: string) {
    const index = this.keys.indexOf(tag);
    // console.log(tag + ' key ' + this.keys);
    if (index >= 0) {
      remove(this.keys, function(e) {
        if (e === tag) { return e; }
      });
    }
  }
  public active() {
    this.helpText = this.HELPTEXT;
  }
  public deactive() {
    this.helpText = {};
  }
  public get(tags) {
    if (Object.keys(this.helpText).length > 0) {
      if (typeof tags === 'string') {
        return (this.keys.indexOf(tags) < 0) ? false : true;
      } else if (Array.isArray(tags)) {
        for (const index in tags) {
          if (this.keys.indexOf(tags[index]) > -1) { return true; }
        }
        return false;
      }
    } else {
      return false;
    }
  }
  public getAll() {
    // console.log(this.keys);
    // console.log(Object.keys(this.helpText));
    if ((Object.keys(this.helpText).length > 0) && (this.keys.length > 0)) {
      return true;
    } else {
      return false;
    }
  }
  public e100(data, tag: string) {
    if (isNil(data) ||
      (typeof data === 'string' && !data) ||
      (Array.isArray(data) && data.length === 0)) {
      this.register(tag);
      return this.helpText['100'];
    } else {
      this.unregister(tag);
      return null;
    }
  }
  public e101(gender: string, tag: string) {
    if (isNil(gender) ||
      (gender !== 'M' &&
        gender !== 'F' &&
        lowerCase(gender) !== 'male' &&
        lowerCase(gender) !== 'female')
    ) {
      this.register(tag);
      return this.helpText['101'];
    } else { this.unregister(tag); return null; }
  }
  public e102(birthday, tag: string) {
    if (moment(birthday) >= moment()) {
      this.register(tag);
      return this.helpText['102'];
    } else { this.unregister(tag); return null; }
  }
  public e103(phone: string, code: any, tag: string) {
    if (!code) { code = 'any'; }
    if (phone && !validator.isMobilePhone(phone, code)) {
      this.register(tag);
      return this.helpText['103'];
    } else { this.unregister(tag); return null; }
  }
  // public e104() {
  //   if (validator.isMobilePhone(phone, code || 'vi-VN')) { return this.helpText['103']; } else { this.unregister(tag); return null; }
  // }
  public e105(email: string, tag: string) {
    if (!validator.isEmail(email)) {
      this.register(tag);
      return this.helpText['105'];
    } else { this.unregister(tag); return null; }
  }
  // public e106() {

  // }
  public e107(username: string, tag: string) {
    if (!username.match(/^[a-zA-Z0-9.\-_$@*!]{6,30}$/)) {
      this.register(tag);
      return this.helpText['107'];
    } else { this.unregister(tag); return null; }
  }
  // public e108() {

  // }
  public e109(term: boolean, tag: string) {
    if (!term) {
      this.register(tag);
      return this.helpText['109'];
    } else { this.unregister(tag); return null; }
  }
  public e110(password, repassword, tag: string) {
    if (!isEqual(password, repassword)) {
      this.register(tag);
      return this.helpText['110'];
    } else { this.unregister(tag); return null; }
  }
  public e111(checkTerm: boolean, tag: string) {
    if (!checkTerm) {
      this.register(tag);
      return this.helpText['111'];
    } else { this.unregister(tag); return null; }
  }

}
