import { Injectable } from '@angular/core';

@Injectable()
export class TableProviderService {
  private data = [
  ];
  private settings = {
    mode: 'inline',
    // hideHeader: true,
    hideSubHeader: true,
    noDataMessage: 'Chưa có dữ liệu',
    actions: {
      columnTitle: 'Xem chi tiết',
      position: 'right',
      edit: false,
      custom: [{ name: 'view', title: '<i class="nb-plus"></i> '}]
    },
    edit: {
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="far fa-save"></i>',
      cancelButtonContent: '<i class="fas fa-ban"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="far fa-trash-alt"></i>',
    },
    pager: {
      display: true,
      perPage: 50,
    },
    attr: {
      class: 'table table-bordered'
    },
    sort: true,
    columns: {
      name: {
        title: 'Tên',
        type: 'html',
        filter: {
          type: 'html',
          config: {
            completer: {
              data: this.data,
              searchFields: 'name.full',
              titleField: 'name.full',
            },
          },
        },
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'name.full',
              titleField: 'name.full',
            },
          },
        },
      },
      website: {
        title: 'Trang chủ',
        filter: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'website',
              titleField: 'website',
            },
          },
        },
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'website',
              titleField: 'website',
            },
          },
        },
      },
      document: {
        title: 'Giới thiệu',
        filter: {
          type: 'text',
          config: {
            completer: {
              data: this.data,
              searchFields: 'document.overview',
              titleField: 'document.overview',
            },
          },
        },
      },
      location: {
        title: 'Địa chỉ',
        filter: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'location.full',
              titleField: 'location.full',
            },
          },
        },
        sortDirection: 'asc',
      }
    }
  };
  constructor() {}

  public get(): Object {
    return this.settings;
  }
  public set(data: Array<Object>): Object {
    this.data = data;
    return this.settings;
  }


}
