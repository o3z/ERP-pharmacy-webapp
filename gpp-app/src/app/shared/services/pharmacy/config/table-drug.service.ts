import { Injectable } from '@angular/core';

@Injectable()
export class TableDrugService {
  private data = [];
  private settings = {
    mode: 'inline',
    // hideHeader: true,
    hideSubHeader: true,
    noDataMessage: 'Chưa có dữ liệu',
    actions: {
      columnTitle: 'Actions',
      add: false,
      position: 'right',
    },
    edit: {
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="far fa-save"></i>',
      cancelButtonContent: '<i class="fas fa-ban"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="far fa-trash-alt"></i>',
    },
    pager: {
      display: true,
      perPage: 50,
    },
    attr: {
      class: 'table table-bordered'
    },
    sort: true,
    columns: {
      name: {
        title: 'Tên thuốc',
        type: 'html',
        filter: {
          type: 'html',
          config: {
            completer: {
              data: this.data,
              searchFields: 'name',
              titleField: 'name',
            },
          },
        },
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'name',
              titleField: 'name',
            },
          },
        },
      },
      type: {
        title: 'Loại thuốc',
        filter: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'type',
              titleField: 'type',
            },
          },
        },
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'type',
              titleField: 'type',
            },
          },
        },
      },
      lot: {
        title: 'Số lô',
        filter: {
          type: 'text',
          config: {
            completer: {
              data: this.data,
              searchFields: 'lot',
              titleField: 'lot',
            },
          },
        },
      },
      exp: {
        title: 'Hạn sử dụng',
        filter: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'exp',
              titleField: 'exp',
            },
          },
        },
        sortDirection: 'asc',
      },
      stock: {
        title: 'Số lượng',
        type: 'completer',
        renderComponent: '',
        filter: {
          type: 'text',
          config: {
            completer: {
              data: this.data,
              searchFields: 'stock',
              titleField: 'stock',
            },
          },
        },
      },
      company: {
        title: 'Nhà sản xuất',
        filter: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'company',
              titleField: 'company',
            },
          },
        },
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'company',
              titleField: 'company',
            },
          },
        },
      },
      'price.out': {
        title: 'Đơn giá',
        filter: {
          type: 'html',
          config: {
            completer: {
              data: this.data,
              searchFields: 'price',
              titleField: 'price',
            },
          },
        },
      },
    }
  };
  constructor() {}

  public get(): Object {
    return this.settings;
  }
  public set(data: Array<Object>): Object {
    this.data = data;
    return this.settings;
  }
}
