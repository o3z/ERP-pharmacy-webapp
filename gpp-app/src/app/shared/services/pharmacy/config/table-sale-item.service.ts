import { Injectable } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

@Injectable()
export class TableSaleItemService {
  private data: LocalDataSource;
  private settings = {
    mode: 'inline',
    // hideHeader: true,
    hideSubHeader: true,
    noDataMessage: 'Chưa có dữ liệu',
    actions: false,
    edit: {
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="far fa-save"></i>',
      cancelButtonContent: '<i class="fas fa-ban"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="far fa-trash-alt"></i>',
    },
    pager: {
      display: true,
      perPage: 50,
    },
    attr: {
      class: 'table table-bordered'
    },
    sort: true,
    columns: {
      name: {
        title: 'Tên thuốc',
        type: 'html',
        filter: {
          type: 'html',
          config: {
            completer: {
              data: this.data,
              searchFields: 'name',
              titleField: 'name',
            },
          },
        },
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: this.data,
              searchFields: 'name',
              titleField: 'name',
            },
          },
        },
      },
      stock: {
        title: 'Số lượng',
        type: 'completer',
        renderComponent: '',
        filter: {
          type: 'text',
          config: {
            completer: {
              data: this.data,
              searchFields: 'stock',
              titleField: 'stock',
            },
          },
        },
      },
      'price.out': {
        title: 'Đơn giá',
        filter: {
          type: 'html',
          config: {
            completer: {
              data: this.data,
              searchFields: 'price',
              titleField: 'price',
            },
          },
        },
      },
      'total': {
        title: 'Thành tiền',
        filter: {
          type: 'html',
          config: {
            completer: {
              data: this.data,
              searchFields: 'price',
              titleField: 'price',
            },
          },
        },
      },
    }
  };
  constructor() {}

  public get(): Object {
    return this.settings;
  }
}
