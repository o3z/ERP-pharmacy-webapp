import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SaleService {
  invoice = new EventEmitter;

  constructor() { }

  public chooseInvoice(invoice: any) {
    this.invoice.emit(invoice);
  }
}
