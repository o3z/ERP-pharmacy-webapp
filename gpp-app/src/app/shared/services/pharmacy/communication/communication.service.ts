import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class CommunicationService {
  user = new EventEmitter();
  whatStep = new EventEmitter();
  drugStore = new EventEmitter();
  help = new EventEmitter();
  menuTop = new EventEmitter();

  constructor() { }
  public changeUser(user: any) {
    this.user.emit(user);
  }
  public registerChangeStep (step: String) {
    this.whatStep.emit(step);
  }
  public registerChangeDrugStore (drugStore: any) {
    this.drugStore.emit(drugStore);
  }
  public changeStateMenuTop(menuTop) {
    this.menuTop.emit(menuTop);
  }
}
