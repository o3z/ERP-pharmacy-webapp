import { Injectable, EventEmitter } from '@angular/core';
import { ApiCoreService } from '../api-core.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class ApiService extends ApiCoreService {

  constructor(
    protected http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {
    super(http);
    const defaults = [];
    this.initHeaderDefault(defaults);
    this.apiHost = environment.apiEndpoint;
  }

  public setHeaders(key: string, value) {
    this.headers = this.headers.append(key, value);
  }

  public login(user: any) {
    return this.callAPI('post', 'pharmacy/login', [], [], user, true);
  }
  public registerStep1(user: any) {
    return this.callAPI('post', 'pharmacy/register', ['step1'], [], user, true);
  }
  public registerStep2(user: any) {
    return this.callAPI('post', 'pharmacy/register', ['step2'], [], user, true);
  }
  public registerStep3(user: any) {
    return this.callAPI('post', 'pharmacy/register', ['step3'], [], user, true);
  }
  public goto(path, data) {
    this.router.navigate(['pharmacy/' + path, data]);
  }
  public getStaff(id: any) {
    return this.callAPI('get', 'pharmacy/account/update', [id], [], [], true);
  }
  public updateStaff(id: any, staff) {
    return this.callAPI('post', 'pharmacy/account/update', [id], [], staff, true);
  }
}
