import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import { environment } from '../../../environments/environment';
import * as _ from 'lodash';

@Injectable()
export class ApiCoreService {
protected apiHost: string;
protected headers: HttpHeaders;
  constructor(
    protected http: HttpClient
  ) { }
  protected initHeaderDefault(defaults: any) {
    /**
     * FIX
     */
    this.headers = new HttpHeaders(defaults);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.message);
    } else {
      if (error.status === 0) {
        error['notification'] = {
          type: 'error',
          message: 'Đã có lỗi xảy ra!'
        };
      }
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(error);
  }

  public callAPI(
    method: string,
    path: string,
    params: Array<any> = [],
    query: Array<string> = [],
    data?: any,
    isJSON: boolean = true
  ) {
    const options = { headers : this.headers };
    let call;
    const apiParams = _.join(params, '/');
    const apiQuery = _.join(query, '&');
    const apiEndpoint = this.apiHost + '/' + path + (apiParams ? ('/' + apiParams) : '') + (apiQuery ? ('?' + apiQuery) : '');
    const reqBody = (typeof data === 'string') ? JSON.stringify(data) : data;
    switch (method) {
      case 'post':
        call = this.http.post(apiEndpoint, reqBody, options);
        break;

      case 'put':
        call = this.http.put(apiEndpoint, reqBody, options);
        break;

      case 'delete':
        call = this.http.delete(apiEndpoint, options);
        break;

      case 'upload':
        call = this.http.post(apiEndpoint, reqBody, options);
        break;

      default:
        // GET
        call = this.http.get(apiEndpoint, options);
        break;
    }
    return call.pipe(retry(3), catchError(this.handleError));
  }

}
