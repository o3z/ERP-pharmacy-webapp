export const environment = {
  production: true,
    apiEndpoint: 'https://erp-pharmacy.herokuapp.com/api',
    switchboardNumber: '+841648375392',
    apiKsEndpoint: 'https://erp-pharmacy.herokuapp.com/'
};
